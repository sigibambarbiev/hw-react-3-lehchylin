import React, { useEffect } from 'react';
import {Product} from "./product";
import s from "./productList.module.scss";
import shoppingCart from "../../image/shoppingCart.png";
import starColor from "../../image/starColor1.png"


export function ProductList(props) {
    const {products, addToCart, addToFavorites, onButton, removeButtonCart, removeButtonStar, counterCart, setCounterCart, counterCartMinus, counterStar, setCounterStar, counterStarMinus} = props;

    // localStorage.setItem('counterStar', '0');
    // localStorage.setItem('counterCart', '0');

    useEffect(() => {
        setCounterStar(Number(localStorage.getItem('counterStar')));
    }, []);

    useEffect(() => {
        setCounterCart(Number(localStorage.getItem('counterCart')));
    },[]);

    useEffect(() => {
        localStorage.setItem('counterCart', counterCart.toString());
    }, [counterCart]);

    useEffect(() => {
        localStorage.setItem('counterStar', counterStar.toString());
    }, [counterStar]);

    function PaintProductCard(product) {

        const {name, price, url, number, volume} = product;
        return (

            <Product
                key={number}
                name={name}
                price={price}
                url={url}
                number={number}
                volume={volume}
                counterCartPlus={counterCartPlus}
                counterCartMinus={counterCartMinus}
                counterStarPlus={counterStarPlus}
                counterStarMinus={counterStarMinus}
                onButton={onButton}
                removeButtonCart={removeButtonCart}
                removeButtonStar={removeButtonStar}
                />
        )
    }

    function counterCartPlus(number) {
        setCounterCart(counterCart + 1);
        addToCart(number);
    }

    function counterStarPlus(number) {
        setCounterStar(counterStar + 1);
        addToFavorites(number);
    }

    return (
        <>
            <div className={s.header}>
                <p className={s.title}>ALCO-MARKET</p>
                <div className={s.headerWrapp}>
                    <div className={s.headerStarWrapp}>
                        <img className={s.shoppingStar} src={starColor}/>
                        {counterStar > 0 && <span className={s.spanStarCounter}>{counterStar}</span>}
                    </div>
                    <div className={s.headerCartWrapp}>
                        <img className={s.shoppingCart} src={shoppingCart}/>
                        {counterCart > 0 && <span className={s.spanCardCounter}>{counterCart}</span>}
                    </div>
                </div>
            </div>
            <div className={s.productList}>
                {products.length > 0 && products.map(product => PaintProductCard(product))}
            </div>
        </>)
}












