import {ProductList} from "./pages/products/productList";
import {Routes, Route} from "react-router-dom";
import {Cart} from "./pages/cart";
import {Favorite} from "./pages/favorite";
import {Layout} from "./pages/layout";
import {useEffect, useState} from "react";
import {request} from "./tools/request";
import s from './App.module.scss';


export function App() {

    const [products, setProducts] = useState([]);
    const [cart, setCart] = useState([]);
    const [favorites, setFavorites] = useState([]);
    const [counterStar, setCounterStar] = useState(0);
    const [counterCart, setCounterCart] = useState(0);

    const fetchProduct = async () => {
        const response = await request();
        if (response) {
            setProducts(response);
        }
    }

    useEffect(() => {
        fetchProduct();
    }, []);

    useEffect(() => {
        setCart(JSON.parse(localStorage.getItem('cartArray')));
    }, []);

    useEffect(() => {
        localStorage.setItem('cartArray', JSON.stringify(cart));
    }, [cart]);

    useEffect(() => {
        setFavorites(JSON.parse(localStorage.getItem('starArray')));
    }, []);

    useEffect(() => {
        localStorage.setItem('starArray', JSON.stringify(favorites));
    }, [favorites]);

    useEffect(() => {
        localStorage.setItem('counterCart', counterCart.toString());
    }, [counterCart]);

    useEffect(() => {
        localStorage.setItem('counterStar', counterStar.toString());
    }, [counterStar]);

    function AddToCart(number) {
        setCart(prevState => [...prevState, number]);
    }

    function AddToFavorites(number) {
        setFavorites(prevState => [...prevState, number]);
    }

    function RemoveFromCart(number) {
        setCart(cart.filter(elem => elem !== number));
    }

    function RemoveFromFavorites(number) {
        setFavorites(favorites.filter(elem => elem !== number));
    }

    function CounterCartMinus(number) {
        setCounterCart(counterCart - 1);
        RemoveFromCart(number);
    }

    function CounterStarMinus(number) {
        setCounterStar(counterStar - 1);
        RemoveFromFavorites(number);
    }

    return (
        <div className={s.app}>
            <Routes>
                <Route path="/" element={<Layout/>}>
                    <Route path="/" element={<ProductList
                        products={products}
                        addToCart={AddToCart}
                        addToFavorites={AddToFavorites}
                        counterCartMinus={CounterCartMinus}
                        counterStarMinus={CounterStarMinus}
                        onButton={true}
                        removeButtonCart={false}
                        removeButtonStar={false}
                        counterCart={counterCart}
                        setCounterCart={setCounterCart}
                        counterStar={counterStar}
                        setCounterStar={setCounterStar}/>}/>
                    <Route path="cart" element={<Cart
                        products={products}
                        cart={cart}
                        onButton={false}
                        removeButtonCart={true}
                        counterCartMinus={CounterCartMinus}/>}/>
                    <Route path="favorite" element={<Favorite
                        products={products}
                        favorites={favorites}
                        onButton={false}
                        removeButtonStar={true}
                        counterStarMinus={CounterStarMinus}/>}/>
                </Route>
            </Routes>
        </div>
    )
}

