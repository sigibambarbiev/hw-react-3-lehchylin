

export async function request () {

    const response = await fetch('./drinks.json');
    const responseJSON = await response.json();

    return responseJSON;

}